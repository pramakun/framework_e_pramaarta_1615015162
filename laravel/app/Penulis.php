<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model
{
    protected $table = 'penulis' ;
    protected $fillable = ['id','nama','notlp','email','alamat'];

	public function buku(){
		return $this->belongsToMany(Buku::class);
	}
  
}
