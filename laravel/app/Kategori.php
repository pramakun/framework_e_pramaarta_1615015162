<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori' ;
    protected $fillable = ['id','deskripsi'];

    public function Buku(){
 		return $this->hasMany(Buku::class);
 	}
}
