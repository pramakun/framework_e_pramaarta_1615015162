<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku' ;
    protected $fillable = ['id','judul','kategori_id','penerbit','tanggal'];

    public function Kategori(){
 		return $this->BelongsTo(Kategori::class);
 	}

 	public function penulis(){
		return $this->belongsToMany(Penulis::class);
	}
}
