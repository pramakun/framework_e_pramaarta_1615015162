<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'pembeli' ;
    protected $fillable = ['id','nama','alamat','notlp'];

    public function Pengguna(){
		return $this->belongsTo(Pengguna::class);
	}

	Public function getUsernameAttribute(){ 
        return $this->pengguna->username; 
	} 
 
	Public function getPasswordAttribute(){ 
        return $this->pengguna->password; 
	} 

}
