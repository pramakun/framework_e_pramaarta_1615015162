<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UjiQueryController extends Controller
{
    // 1. Retrieving

	//Mengambil semua data dari table 
    Public function index(){ 
       $pengguna = Pengguna::all(); 
       dd($pengguna); 
	}

	//Mengambil sebaris data dari table 
	/*Public function index(){ 
       $pengguna = Pengguna::all()->where(‘username’,’Taufik’)->first(); 
       dd($pengguna); 
	}*/

	//Mengambil satu kolom data dari table 
	/*Public function index(){ 
       $pengguna = Pengguna::all()->pluck(‘username’); 
       dd($pengguna); 
	}*/

	//menampilan lebih dari satu kolom dengan menambahkan koma dan nama kolom yang ingin ditampilkan 
	/*Public function index(){ 
      $pengguna = Pengguna::all()->pluck(‘username’,’password’); 
      dd($pengguna); 
	} */


	// 2. Agregat

	/*Public function index(){ 
      $pengguna = Pengguna::all()->count(); 
      dd($pengguna); 
	} */
	//Count bisa diubah jadi Max('harga'), sum(), dsb


	// 3. Select

	//menampilkan hanya satu atau lebih kolom sesuai dengan keinginan 
	/*Public function index(){ 
      $pengguna = Pengguna::select(‘username’,’ password’)->get(); 
      dd($pengguna); 
	}*/

	// menggunakan perintah distinct  
	/*Public function index(){ 
     $pengguna = Pengguna::select(‘level’)->distinct()->get(); 
     dd($pengguna); 
	}*/

}
