<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penulis;
use App\Http\Requests;

class PenulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function awal()
    {
        $penulis=Penulis::all();
        return view ('penulis.app',compact('penulis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah()
    {
        return view('penulis.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan(Request $input)
    {
        $this->validate($input, array 
        ( 
        'nama' => 'required',
        'notlp' => 'required|integer',
        'email' => 'required',
        'alamat' => 'required', 
        )); 
        

        $penulis = new Penulis();
        $penulis->nama=$input->nama;
        $penulis->notlp=$input->notlp;
        $penulis->email=$input->email;
        $penulis->alamat=$input->alamat;
        $status = $penulis->save();
        return redirect('penulis')->with(['status'=>$status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lihat($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penulis=Penulis::find($id);
        return view ('penulis.edit')->with(array('penulis'=>$penulis));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $input)
    {
        $this->validate($input, array 
        ( 
        'nama' => 'required',
        'notlp' => 'required|integer',
        'email' => 'required',
        'alamat' => 'required', 
        )); 
        
        $penulis=Penulis::find($id);
        $penulis->nama=$input->nama;
        $penulis->notlp=$input->notlp;
        $penulis->email=$input->email;
        $penulis->alamat=$input->alamat;
        $status = $penulis->save();
        return redirect('penulis')->with(['status'=>$status]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $penulis=Penulis::find($id);
        $penulis->delete();
        return redirect('penulis');
    }
}
