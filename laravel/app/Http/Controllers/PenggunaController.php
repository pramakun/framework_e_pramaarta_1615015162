<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pengguna;

class PenggunaController extends Controller
{
    public function awal(){
    	return "Halo dari PenggunaController";
    }

    public function simpan(Request $input){
        $this->validate($input, array 
        ( 
            'username' => 'required', 
            'password' => 'required', 
        )); 

    	$pengguna = new Pengguna;
    	// $pengguna->username = 'maikel';
    	// $pengguna->password = 'jordon';
    	// $pengguna->save();
    	// return "data atas nama {$pengguna->username} telah tersimpan";
        $pengguna->username=$input->username;
        $pengguna->password=$input->password;
        $status = $pengguna->save();
    }

    public function tes(){
    	$pengguna = Pengguna(['username'=>'tes','password'=>'tes']);
    Pengguna::create($pengguna);
    }
    
}
