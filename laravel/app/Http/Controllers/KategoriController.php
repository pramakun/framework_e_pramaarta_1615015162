<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Http\Requests;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function awal()
    {
       $kategori = Kategori::all();
        return view('kategori.kategori',compact('kategori'));
    }

    /**
     * Show the form for creating a new rwesource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah()
    {
        return view('kategori.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan(Request $input)
    {
        $this->validate($input, array 
        ( 
            'deskripsi' => 'required', 
        )); 

        $kategori = new Kategori();
        $kategori->deskripsi = $input->deskripsi;
        $kategori->save();
        return redirect('kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lihat($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.editkategori')->with(array('kategori'=>$kategori));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update( $id, Request $input)
    {
        $kategori = Kategori::find($id);
        return view ('buku.editkategori')->with(array('kategori'=>$kategori));
    }*/

    public function update($id, Request $input)
    {
        $this->validate($input, array 
        ( 
            'deskripsi' => 'required', 
        ));
        
        $kategori = Kategori::find($id);
        $kategori->deskripsi = $input->deskripsi;
        $kategori->save();
        return redirect('kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $kategori = Kategori::find($id);
        $kategori->delete();
        return redirect('kategori');
    }
}
